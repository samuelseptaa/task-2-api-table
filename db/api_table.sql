-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.7.33 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             11.2.0.6213
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Dumping database structure for api_table
DROP DATABASE IF EXISTS `api_table`;
CREATE DATABASE IF NOT EXISTS `api_table` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `api_table`;

-- Dumping structure for table api_table.mahasiswas
DROP TABLE IF EXISTS `mahasiswas`;
CREATE TABLE IF NOT EXISTS `mahasiswas` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nim` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table api_table.mahasiswas: ~50 rows (approximately)
/*!40000 ALTER TABLE `mahasiswas` DISABLE KEYS */;
INSERT INTO `mahasiswas` (`id`, `nama`, `nim`, `alamat`, `created_at`, `updated_at`) VALUES
	(1, 'Katelyn Rau', '118067', '66626 Alexandrine Turnpike Apt. 862\nClarissafurt, ME 65971', '2022-07-06 03:56:36', '2022-07-06 03:56:36'),
	(2, 'Prof. Ora Koepp Sr.', '118041', '2059 Shaina Meadow Suite 939\nNorth Jenifer, DE 83688-7682', '2022-07-06 03:56:36', '2022-07-06 03:56:36'),
	(3, 'Cheyanne Rath', '118030', '148 Gage Inlet Suite 808\nOlinmouth, NJ 16848-8444', '2022-07-06 03:56:36', '2022-07-06 03:56:36'),
	(4, 'Dr. Mazie Connelly I', '118014', '998 Casper Track Apt. 087\nWatsicaland, NM 65611', '2022-07-06 03:56:36', '2022-07-06 03:56:36'),
	(5, 'Mr. Amir Cremin', '118083', '4290 Lenora Common\nNorth Kaiamouth, AK 91483-4941', '2022-07-06 03:56:36', '2022-07-06 03:56:36'),
	(6, 'Levi Jakubowski', '118094', '154 Allan Greens Apt. 219\nKentontown, WI 78950', '2022-07-06 03:56:36', '2022-07-06 03:56:36'),
	(7, 'Prof. Jennie VonRueden', '118001', '90214 Dibbert Throughway Apt. 914\nWest Gabriel, NJ 24864', '2022-07-06 03:56:36', '2022-07-06 03:56:36'),
	(8, 'Prof. Cooper Feil', '118099', '732 Schaden Crossing\nEmersonchester, SC 34450', '2022-07-06 03:56:36', '2022-07-06 03:56:36'),
	(9, 'Mr. Waylon Lindgren Sr.', '118063', '1484 Wolff Highway Suite 473\nPort Sedrick, NV 34966-3995', '2022-07-06 03:56:36', '2022-07-06 03:56:36'),
	(10, 'Ardith Hills', '118018', '7536 Wyman Pike\nLake Hillardchester, NC 76652', '2022-07-06 03:56:36', '2022-07-06 03:56:36'),
	(11, 'Prof. Ardith Baumbach', '118034', '37374 Wintheiser Corners Suite 885\nNorth Johnsonshire, MN 99901', '2022-07-06 03:56:36', '2022-07-06 03:56:36'),
	(12, 'Sandra Gibson', '118042', '79831 Mosciski Vista Apt. 903\nJuneland, SC 31202', '2022-07-06 03:56:36', '2022-07-06 03:56:36'),
	(13, 'Prof. Pete Grady', '118100', '54475 Trantow Lodge\nZeldamouth, ND 75875-8918', '2022-07-06 03:56:36', '2022-07-06 03:56:36'),
	(14, 'Alva Koepp', '118059', '90687 Antonette Terrace\nPort Alexandromouth, ME 26506', '2022-07-06 03:56:36', '2022-07-06 03:56:36'),
	(15, 'Mr. Kris VonRueden II', '118013', '52746 Dario Hills\nCronaside, GA 36400', '2022-07-06 03:56:36', '2022-07-06 03:56:36'),
	(16, 'Kaleigh Dooley', '118057', '13071 Fritsch Circle\nEast Ariel, NY 34379', '2022-07-06 03:56:36', '2022-07-06 03:56:36'),
	(17, 'Bonita Fritsch', '118076', '847 Hackett Union\nWest Julien, PA 81631-3876', '2022-07-06 03:56:36', '2022-07-06 03:56:36'),
	(18, 'Una Batz', '118047', '722 Boyle Ford Suite 749\nEast Carlee, AL 80992', '2022-07-06 03:56:36', '2022-07-06 03:56:36'),
	(19, 'Leonora Thompson Sr.', '118075', '553 Predovic Avenue\nAbbieville, AZ 19857', '2022-07-06 03:56:36', '2022-07-06 03:56:36'),
	(20, 'Keara Steuber', '118054', '78824 Wilfredo Ports Suite 647\nMarinamouth, ND 10974-3864', '2022-07-06 03:56:36', '2022-07-06 03:56:36'),
	(21, 'Litzy Witting', '118096', '87550 Carmella Rest\nSchultzborough, IN 70309-6016', '2022-07-06 03:56:36', '2022-07-06 03:56:36'),
	(22, 'Darrick Goodwin', '118082', '493 McDermott Lakes\nPhyllisland, UT 48908-0736', '2022-07-06 03:56:36', '2022-07-06 03:56:36'),
	(23, 'Prof. Hershel Will', '118028', '29764 Ethyl Mount\nWildermanchester, AL 60695', '2022-07-06 03:56:36', '2022-07-06 03:56:36'),
	(24, 'Lester Hartmann', '118058', '4497 Medhurst Extension\nPort Rahul, NM 55989-4035', '2022-07-06 03:56:36', '2022-07-06 03:56:36'),
	(25, 'Reece Kuhic', '118015', '391 Ondricka Spur Suite 795\nSouth Alize, MI 42436-9224', '2022-07-06 03:56:36', '2022-07-06 03:56:36'),
	(26, 'Alexandro Macejkovic', '118093', '2151 Balistreri Path Apt. 411\nKutchton, CO 65646', '2022-07-06 03:56:36', '2022-07-06 03:56:36'),
	(27, 'Prof. Randy Lakin', '118019', '4861 Jenifer Wells\nJohnsside, NV 04146-2059', '2022-07-06 03:56:36', '2022-07-06 03:56:36'),
	(28, 'Dr. Priscilla Halvorson', '118086', '1356 Jakubowski Underpass Suite 742\nNorth Willyport, MO 16709', '2022-07-06 03:56:36', '2022-07-06 03:56:36'),
	(29, 'Alford Runolfsdottir', '118085', '8510 Sister Circles\nNew Marietta, SD 68516-5684', '2022-07-06 03:56:36', '2022-07-06 03:56:36'),
	(30, 'Alta Ritchie', '118074', '74239 Schamberger Island Suite 830\nEast Stone, HI 19032-7605', '2022-07-06 03:56:36', '2022-07-06 03:56:36'),
	(31, 'Albertha Maggio', '118008', '80189 Chad Ridge Apt. 804\nWest Lue, ME 46178', '2022-07-06 03:56:36', '2022-07-06 03:56:36'),
	(32, 'Ms. Katelynn Okuneva', '118029', '2825 Jaylen Key\nAlyceberg, NH 51657-5900', '2022-07-06 03:56:36', '2022-07-06 03:56:36'),
	(33, 'Prof. Rylee Gaylord V', '118002', '84432 Bauch Way Suite 979\nHeathcotemouth, MS 03556', '2022-07-06 03:56:36', '2022-07-06 03:56:36'),
	(34, 'Ulises Heidenreich', '118048', '86490 Marvin Course\nHymanhaven, DE 05011-5493', '2022-07-06 03:56:36', '2022-07-06 03:56:36'),
	(35, 'Einar Krajcik', '118050', '22971 Ciara Stream Apt. 547\nPacochaburgh, IN 09536-4342', '2022-07-06 03:56:36', '2022-07-06 03:56:36'),
	(36, 'Lexus Wiegand', '118077', '6688 Giovanna Terrace Apt. 802\nNew Nakia, MS 66562', '2022-07-06 03:56:36', '2022-07-06 03:56:36'),
	(37, 'Christa Ritchie', '118078', '5563 Crona Village\nNorth Nathanialstad, PA 33968', '2022-07-06 03:56:36', '2022-07-06 03:56:36'),
	(38, 'Alexys Donnelly Jr.', '118062', '97989 Margarette Ferry Apt. 817\nPaulchester, AR 48367', '2022-07-06 03:56:36', '2022-07-06 03:56:36'),
	(39, 'Prof. Mavis Schmitt', '118004', '6186 Eldred Lights Apt. 719\nNorth Leopold, RI 29708-5453', '2022-07-06 03:56:36', '2022-07-06 03:56:36'),
	(40, 'Dr. Payton Quitzon PhD', '118005', '483 Schaefer Walk\nVanessatown, WY 71066-5050', '2022-07-06 03:56:36', '2022-07-06 03:56:36'),
	(41, 'Prof. Dahlia Brekke', '118052', '59265 Armstrong Ramp\nNorth Griffinfurt, CO 18153-4444', '2022-07-06 03:56:36', '2022-07-06 03:56:36'),
	(42, 'Yasmin Hills', '118089', '8406 Zelma Spring\nEast Shyannville, TN 77217', '2022-07-06 03:56:36', '2022-07-06 03:56:36'),
	(43, 'Adolf Bayer IV', '118027', '17742 Christophe Cliffs\nVincenthaven, TX 06654-6138', '2022-07-06 03:56:36', '2022-07-06 03:56:36'),
	(44, 'Raheem Thiel', '118012', '62776 King Path\nNorth Damionport, IL 43540', '2022-07-06 03:56:36', '2022-07-06 03:56:36'),
	(45, 'Prof. Lorine DuBuque', '118016', '3817 Spinka Harbor\nNew Kaleyton, AK 75215-8538', '2022-07-06 03:56:36', '2022-07-06 03:56:36'),
	(46, 'Gertrude Feil', '118087', '9868 Whitney Village Apt. 072\nEast Novaview, RI 51918-1656', '2022-07-06 03:56:36', '2022-07-06 03:56:36'),
	(47, 'Irwin Fay V', '118073', '53082 Carlos Field Apt. 021\nLailashire, MD 76270-9016', '2022-07-06 03:56:36', '2022-07-06 03:56:36'),
	(48, 'Angela Hirthe', '118039', '46071 Austyn Light\nWaelchiview, KS 18618', '2022-07-06 03:56:36', '2022-07-06 03:56:36'),
	(49, 'Nels Hansen', '118032', '9873 Fahey Court\nShanebury, TX 68049', '2022-07-06 03:56:36', '2022-07-06 03:56:36'),
	(50, 'Dr. Oral Bechtelar Sr.', '118060', '7704 Jerald Pine Suite 141\nLake Laurianneburgh, OR 88388', '2022-07-06 03:56:36', '2022-07-06 03:56:36');
/*!40000 ALTER TABLE `mahasiswas` ENABLE KEYS */;

-- Dumping structure for table api_table.migrations
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table api_table.migrations: ~2 rows (approximately)
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2019_12_14_000001_create_personal_access_tokens_table', 1),
	(2, '2022_07_06_010226_create_mahasiswas_table', 1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Dumping structure for table api_table.personal_access_tokens
DROP TABLE IF EXISTS `personal_access_tokens`;
CREATE TABLE IF NOT EXISTS `personal_access_tokens` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table api_table.personal_access_tokens: ~0 rows (approximately)
/*!40000 ALTER TABLE `personal_access_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `personal_access_tokens` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
