<?php

namespace App\Http\Controllers;

use App\Models\Mahasiswa as ModelsMahasiswa;
use Illuminate\Http\Request;
use PhpParser\Node\Stmt\Echo_;

class Mahasiswa extends Controller
{
    public function index_post()
    {
        return ModelsMahasiswa::all();
    }

    public function table_mahasiswa(Request $request)
    {
        $query = ModelsMahasiswa::query();
        if ($search = $request->input('search')) {
            $query
                ->where('nama', 'like', '%' . $search . '%')
                ->orWhere('nim', 'like', '%' . $search . '%');
        }
        $total = $query->count();
        $perPage = 10;
        $page = intval($request->input('page', 1));
        $result = $query->offset(($page - 1) * $perPage)->limit($perPage)->get();

        return [
            'start_at' => $page * $perPage - 9,
            'data' => $result,
            'total' => $total,
            'page' => $page,
            'total_page' => ceil($total / $perPage)
        ];
    }

    public function mahasiswa_get(Request $request)
    {
        $query = ModelsMahasiswa::query();
        $id = $request->input('id');
        if ($id == null) {
            return response(
                [
                    'status' => 'Error',
                    'message' => [
                        'title' => 'Gagal',
                        'body' => "Tidak ada id yang dikirimkan!"
                    ],
                ],
                400
            );
        }
        $query
            ->where('id', $id);
        if ($query->first() == null) {
            return response(
                [
                    'status' => 'Error',
                    'message' => [
                        'title' => 'Gagal',
                        'body' => "Tidak ada data dengan id tersebut!"
                    ],
                ],
                400
            );
        }
        return response(
            [
                'data' => $query->first(),
            ],
            200
        );
    }
    public function mahasiswa_edit(Request $request)
    {
        $query = ModelsMahasiswa::query();
        $id = $request->input('id__');
        if ($id == null) {
            return response(
                [
                    'status' => 'Error',
                    'message' => [
                        'title' => 'Gagal',
                        'body' => "Tidak ada id yang dikirimkan!"
                    ],
                ],
                400
            );
        }
        $query
            ->where('id', $id)
            ->update([
                'nama' => $request->input('nama__'),
                'nim' => $request->input('nim__'),
                'alamat' => $request->input('alamat__'),
            ]);
        return response(
            [
                'status' => 'OK',
                'message' => [
                    'title' => 'Berhasil',
                    'body' => "Data diubah"
                ],
            ],
            200
        );
    }
    public function mahasiswa_delete(Request $request)
    {
        // dd($request->input());

        $query = ModelsMahasiswa::query();
        $id = $request->input('id');
        if ($id == null) {
            return response(
                [
                    'status' => 'Error',
                    'message' => [
                        'title' => 'Gagal',
                        'body' => "Tidak ada id yang dikirimkan!"
                    ],
                ],
                400
            );
        }
        $query
            ->find($id)
            ->delete();
        return response(
            [
                'status' => 'OK',
                'message' => [
                    'title' => 'Berhasil',
                    'body' => "Data dihapus"
                ],
            ],
            200
        );
    }

    public function mahasiswa_store(Request $request)
    {
        if ($request->input() == null) {
            return response(
                [
                    'status' => 'Error',
                    'message' => [
                        'title' => 'Gagal',
                        'body' => "Tidak ada data yang dikirimkan!"
                    ],
                ],
                400
            );
        }
        $query = ModelsMahasiswa::query();

        $query
            ->where('nim', $request->input('nim'));
        if ($query->first() != null) {
            return response(
                [
                    'status' => 'Error',
                    'message' => [
                        'title' => 'Gagal',
                        'body' => "NIM Sudah Ada!"
                    ],
                ],
                400
            );
        }

        $query->insert([
            'nama' => $request->input('nama'),
            'nim' => $request->input('nim'),
            'alamat' => $request->input('alamat')
        ]);

        return response(
            [
                'status' => 'Sukses',
                'message' => [
                    'title' => 'Berhasil',
                    'body' => "Data ditambahkan!"
                ],
            ],
            201
        );
    }
}
