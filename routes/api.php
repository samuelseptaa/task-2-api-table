<?php

use App\Http\Controllers\Mahasiswa;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('mahasiswa', [Mahasiswa::class, 'index_post']);
Route::post('mahasiswa/get', [Mahasiswa::class, 'table_mahasiswa']);
Route::post('mahasiswa_store', [Mahasiswa::class, 'mahasiswa_store']);
Route::post('mahasiswa_get', [Mahasiswa::class, 'mahasiswa_get']);
Route::post('mahasiswa_edit', [Mahasiswa::class, 'mahasiswa_edit']);
Route::delete('mahasiswa_delete', [Mahasiswa::class, 'mahasiswa_delete']);
