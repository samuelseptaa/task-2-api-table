<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <title>Hello, API!</title>
</head>

<body class="container-fluid">
    <div class="mt-5">
        <div class="row mb-3">
            <div class="col-4">
                <div class="input-group mb-3">
                    <input type="text" class="form-control" placeholder="" name="search" id="search" aria-label="" aria-describedby="basic-addon1">
                    <button class="btn btn-outline-secondary" id="cari" type="button" id="button-addon2">Cari</button>
                </div>
            </div>
            <div class="col-8">
                <div class="text-end">
                    <button class="btn btn-outline-secondary" id="tambah" type="button">Tambah</button>
                </div>
            </div>
        </div>
        <table id="" class="table table-responsive table-bordered">
            <thead>
                <tr>
                    <th>Nomor</th>
                    <th>Nama</th>
                    <th>NIM</th>
                    <th>Alamat</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody id="row_mahasiswa">

            </tbody>

        </table>
    </div>
    <nav aria-label="Page navigation example">
        <ul class="pagination" id="pagination">
        </ul>
    </nav>
    <div class="modal fade" id="modal_edit" tabindex="-1" aria-labelledby="modal_editLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modal_editLabel">Edit</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form action="" id="edit">
                        <input type="hidden" name="id__" id="id__">
                        <div class="form-group mb-3">
                            <label class="col-form-label">Nama</label>
                            <input required type="text" id="nama__" name="nama__" class="form-control" placeholder="">
                        </div>
                        <div class="form-group mb-3">
                            <label class="col-form-label">NIM</label>
                            <input required type="number" id="nim__" name="nim__" class="form-control" placeholder="">
                        </div>
                        <div class="form-group mb-3">
                            <label class="col-form-label">Alamat</label>
                            <input required type="text" id="alamat__" name="alamat__" class="form-control" placeholder="">
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Save Data</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal_tambah" tabindex="-1" aria-labelledby="modal_tambahLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modal_tambahLabel">Tambah</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form action="" id="tambah_form">
                        <div class="form-group mb-3">
                            <label class="col-form-label">Nama</label>
                            <input required type="text" id="nama" name="nama" class="form-control" placeholder="">
                        </div>
                        <div class="form-group mb-3">
                            <label class="col-form-label">NIM</label>
                            <input required type="number" id="nim" name="nim" class="form-control" placeholder="">
                        </div>
                        <div class="form-group mb-3">
                            <label class="col-form-label">Alamat</label>
                            <input required type="text" id="alamat" name="alamat" class="form-control" placeholder="">
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Save Data</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</body>
<footer>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
</footer>

<script>
    function getDatatable(response) {
        const data = response.data;
        const total_page = response.total_page;
        const page = response.page
        let start_at = response.start_at;
        let html = '';
        data.forEach((element) => {
            html += `<tr>
                            <td>${start_at}</td>
                            <td>${element.nama}</td>
                            <td>${element.nim}</td>
                            <td>${element.alamat}</td>
                            <td> <button type="button" onclick="edit(${element.id})" class="btn btn-primary btn-sm">Edit</button>
                            <button type="button" onclick="hapus(${element.id})" class="btn btn-danger btn-sm">Hapus</button>
                            </td>
                        </tr>`;
            start_at++;
        });

        let html2 = '';
        for (let i = 1; i <= total_page; i++) {
            if (i == page)
                html2 += `<li class="page-item active"><button onclick=pageChange(${i}) disabled class="page-link">${i}</button></li>`;
            else
                html2 += `<li class="page-item"><button onclick=pageChange(${i})  class="page-link">${i}</button></li>`;
        }
        $("#pagination").html(html2);
        $("#row_mahasiswa").html(html);
    }

    $.ajax({
        type: "post",
        url: `/api/mahasiswa/get`,
        success: getDatatable,
    });

    function pageChange(page) {
        $.ajax({
            type: "post",
            url: `/api/mahasiswa/get`,
            data: {
                page: page,
                search: $("#search").val()
            },
            success: getDatatable,
        });
    }
    $("#cari").click(function(e) {
        $.ajax({
            type: "post",
            url: `/api/mahasiswa/get`,
            data: {
                search: $("#search").val()
            },
            success: getDatatable,
        });
    });

    function edit(id) {
        $.ajax({
            type: "POST",
            data: {
                id: id
            },
            url: '/api/mahasiswa_get',
            success: function(response) {
                console.log(response);
                const data = response.data;
                $("#id__").val(data.id);
                $("#nama__").val(data.nama);
                $("#nim__").val(data.nim);
                $("#alamat__").val(data.alamat);
            },
            error: function(jqXHR, textstatus, errorThrown) {
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: textstatus,
                });
            }
        });
        $("#modal_edit").modal('show');
    }

    $("#edit").submit(function(e) {
        e.preventDefault();
        let formdata = new FormData(this);
        $.ajax({
            type: "POST",
            data: formdata,
            processData: false,
            contentType: false,
            url: '/api/mahasiswa_edit',
            success: function(response) {
                Swal.fire({
                    icon: 'success',
                    title: response.message.title,
                    text: response.message.body,
                }).then((result) => {
                    if (result.isConfirmed) location.reload();
                });
            },
            error: function(jqXHR, textstatus, errorThrown) {
                Swal.fire({
                    icon: 'error',
                    title: textstatus,
                    text: jqXHR.responseJSON.message.body,
                });
            }
        });
    });

    function hapus(id) {
        Swal.fire({
            icon: 'warning',
            text: 'Apakah Anda ingin menghapus data ini?',
            showCancelButton: true,
            confirmButtonText: 'Hapus',
            denyButtonText: `Batal`,
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    type: "DELETE",
                    data: {
                        id: id
                    },
                    processData: true,
                    dataType: 'JSON',
                    url: 'api/mahasiswa_delete',
                    success: function(response) {
                        Swal.fire({
                            icon: 'success',
                            title: response.message.title,
                            text: response.message.body,
                        }).then((result) => {
                            if (result.isConfirmed) location.reload();
                        });
                    },
                    error: function(jqXHR, textstatus, errorThrown) {
                        Swal.fire({
                            icon: 'error',
                            title: textstatus,
                            text: jqXHR.responseJSON.message.body,
                        });
                    }
                });
            }
        });
    }
    $("#tambah").click(function(e) {
        $("#modal_tambah").modal('show');
    });

    // $("#tambah").submit(function(e) {
    //     e.preventDefault();
    //     let formdata = new FormData(this);
    //     $.ajax({
    //         type: "POST",
    //         data: formdata,
    //         processData: false,
    //         contentType: false,
    //         url: '/api/mahasiswa_store',
    //         success: function(response) {
    //             Swal.fire({
    //                 icon: 'success',
    //                 title: response.message.title,
    //                 text: response.message.body,
    //             }).then((result) => {
    //                 if (result.isConfirmed) location.reload();
    //             });
    //         },
    //         error: function(jqXHR, textstatus, errorThrown) {
    //             Swal.fire({
    //                 icon: 'error',
    //                 title: textstatus,
    //                 text: jqXHR.responseJSON.message.body,
    //             });
    //         }
    //     });
    // });
    $("#tambah_form").submit(function(e) {
        e.preventDefault();
        let formdata = new FormData(this);
        $.ajax({
            type: "POST",
            data: formdata,
            processData: false,
            contentType: false,
            url: '/api/mahasiswa_store',
            success: function(response) {
                Swal.fire({
                    icon: 'success',
                    title: response.message.title,
                    text: response.message.body,
                }).then((result) => {
                    if (result.isConfirmed) location.reload();
                });
            },
            error: function(jqXHR, textstatus, errorThrown) {
                Swal.fire({
                    icon: 'error',
                    title: textstatus,
                    text: jqXHR.responseJSON.message.body,
                });
            }
        });
    })
</script>

</html>